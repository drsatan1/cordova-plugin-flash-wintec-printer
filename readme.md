This plugin was developed to access the printer of a Chinese manufactured device specifically for our company.

It uses files they gave us in the form of .so files, that we load with cordova using System.loadlibrary() or something. That in turn hits the com port and sends stuff to printer.

The printer sdk supports printing a bmp image, text, and multiple barcode styles. We also send data to the printer in the form of 32 characters at a time. The printer doesn't do any buffering of its own, and if we don't do this we get overflows on slips of about 4kb in size or more. I think this includes the size of the bitmap and stuff. 

Anyway, load with cordova, send your shit to the new object on your window, bobs your onkel.

---- ALSO this is some useful comments:

/** \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
 *   arg0: Accepts a message, containing the string to print. We use ; as the CRLF character.
*   <logo/> tells us to print the FLASH cow logo.
*   <barcode> and </barcode> around a valid barcode to print it.
*   Calls callbackContext.error() when fails and callbackContext.success() when succeeds.
**/

// Map typeMap = new HashMap();
// typeMap.put("UPC_A", 0);
// typeMap.put("UPC_E", 1);
// typeMap.put("JAN13",2);
// typeMap.put("JAN8", 3);
// typeMap.put("CODE39", 4);
// typeMap.put("ITF", 5);
// typeMap.put("CODABAR", 6);
// return typeMap;
//<barcode><type>6</type><payload>12334!not a valid CODABAR  barcode im lazy!5345345</payload></barcode>

exports.print = function(arg0, success, error) {
    exec(success, error, "FlashWintecPrinter", "print", [arg0]);
};

/* \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
*  @logoPath: A path (excluding /mnt/ part) to the logo that we'll be using
*   @devicePath: The internal path to the printer (dev/ttySAC1 for instance)
*   returns: nothing.
*/
exports.initPrinter = function(arg0, arg1, success, error) {
    exec(success, error, "FlashWintecPrinter", "initPrinter", [arg0, arg1])
};
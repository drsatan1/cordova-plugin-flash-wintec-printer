package com.flash.cordova.plugin;

import org.apache.cordova.CordovaPlugin;

import java.util.Map;
import java.util.HashMap;
import java.io.File;
import android.util.Log;
import android.os.Build;

import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cn.wintec.wtandroidjar.ComIO;
import cn.wintec.wtandroidjar.Printer;

/**
 * This class prints for us.
 * Moova touch. Yeezy bruh. Kanye 2020
 * Simon Visser. Never forget.
 */
public class FlashWintecPrinter extends CordovaPlugin {
    //Constants

    Printer printer = null;

    String DEVICE_PATH = "Uninitialised"; /// "dev/ttySAC1"
    final String CHARSET = "gbk";

    final String BARCODE_START = "<barcode>";
    final String BARCODE_END = "</barcode>";

    final String TYPE_START = "<type>";
    final String TYPE_END = "</type>";

    final String PAYLOAD_START = "<payload>";
    final String PAYLOAD_END = "</payload>";

    final String LOGO_TAG = "<logo/>";

    final String BOLD_TAG = "</b>";

    final String HEADER1_TAG = "</h1>";

    final String HEADER2_TAG = "</h2>";

    String LOGO_PATH = "Uninitialised"; //"/sdcard/ic_cow.bmp" <<<Moova touch 4.2  ;
    // final String LOGO_PATH = "/internal_sd/ic_cow.bmp" <<<Moova touch 5.1 ;
    final int BITMAP_SIZE = 0;
    final int BITMAP_ALIGN_LEFT = 0;
    final int BITMAP_ALIGN_CENTER = 1;

    final int HEADER_GAP = 1;

    final byte BARCODE_AREA_LENGTH = 10;
    final byte BARCORE_AREA_WIDTH = 10;

    final int BARCODE_HEIGHT = 80;
    final int BARCODE_WIDTH = 4;

    final int HRI_DONT_PRINT = 0;
    final int HRI_TOP = 1;
    final int HRI_BOTTOM = 2;
    final int HRI_TOP_AND_BOTTOM = 3;

    final int BOTTOM_LINES = 6;

    final int LINES_AFTER_BARCODE_REPEAT_PRINT = 2;
    //End constants

    CallbackContext context = null;

    private String deviceType = "";
    private String fontSize = "0";
    private boolean printBarcode;
    private boolean repeating = false;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        context = callbackContext;
        if (action.equals("print")) {
            String message = args.getString(0);
            this.print(message, callbackContext);
            return true;
        } else if (action.equals("initPrinter")) {
            String arg1 = args.getString(0);
            String arg2 = args.getString(1);
            String arg3 = args.getString(2);
            String arg4 = args.getString(3);

            this.initPrinter(arg1, arg2, arg3, arg4);
            return true;
        }
        return false;
    }

    /*  @logoPath: A path (excluding /mnt/ part) to the logo that we'll be using
    *   @devicePath: The internal path to the printer (dev/ttySAC1 for instance)
    *   @fontSize: Font size as string. Valid values: 0, normal. 1, bold all. 2, double height, 3 double width + height
    *   returns: nothing.
    */
    private void initPrinter(String logoPath, String devicePath, String fontSize, String printBarcode) {
        repeating = false;
        this.printBarcode = !(printBarcode.equals("false"));
        ComIO.Baudrate baud = ComIO.Baudrate.valueOf("BAUD_38400");
        this.LOGO_PATH = logoPath;
        this.DEVICE_PATH = devicePath;
        // Initialising the printer.
        ComIO.Param param = new ComIO(DEVICE_PATH).new Param();
        param.baudrate = baud;
        new ComIO(DEVICE_PATH).setSerialBaudrate(param.baudrate);
        printer = new Printer(DEVICE_PATH, baud);

        if (Build.DISPLAY.equals("rk3188-userdebug 5.1.1 LMY47V eng.root.20160822.131547 VL6.1.2")) {
            deviceType = "TouchPro";
        }
        this.fontSize = fontSize;
        //End init
    }

    /**  Accepts a message, containing the string to print. We use ; as the CRLF character.
    *   <logo/> tells us to print the FLASH cow logo.
    *   <barcode> and </barcode> around a valid barcode to print it.
    *   Calls callbackContext.error() when fails and callbackContext.success() when succeeds.
    **/
    private void print(String message, CallbackContext callbackContext) {
        if (printer == null) {
            context.error("Printer has not been initialised.");
        } else {
            if (message != null && message.length() > 0) {
                String x = message.replaceAll(";", "\r\n").replaceAll("~", "");
                String[] lines = x.split("\\r?\\n");

                if (deviceType.equals("TouchPro")) { //We need it to be big on the TouchPro. And normal on other stuff.
                    printer.PRN_EnableFontDoubleWidth();
                } else {
                    printer.PRN_SetCharRightSpace(2);
                    printer.PRN_DisableFontDoubleHeight();
                    printer.PRN_DisableFontDoubleWidth();
                    // printer.PRN_EnableFontDoubleHeight();
                }

                //Start printing stuff.
                if (printer.PRN_GetPaperStatus()) {
                    // printer.PRN_PrintBuffer();
                    int indices = 0;
                    //We have the stuff we need to print, and we have paper. Let's print.
                    for (String line : lines) {
                        String toPrint = lines[indices];
                        if (line == null) {
                            line = "";
                        }

                        Double fooBar = (lines[indices].length() / 27.0); //fooBar == bloc == How many blocks of 32 characters we have. Rounding up.
                        Double blocks = Math.ceil(fooBar);
                        Integer bloc = blocks.intValue();

                        if (bloc == 0) { //If no blocks, print an empty line.
                            printer.PRN_PrintAndFeedLine(1);
                        }

                        for (int xy = 0; xy < bloc; xy++) { //Will trigger 0 times if we have no characters to print.
                            String foo = toPrint.substring(0, Math.min(27, toPrint.length())); //foo is one block of 32 (or less) characters.
                            Log.d(null, "LEFT: " + toPrint);
                            if (toPrint.length() > 27) {
                                toPrint = toPrint.substring(27); //Did I mess this up? We cut away the part that we put into foo here.
                            }

                            if (line.contains(LOGO_TAG)) { //Logo found. print logo.
                                printLogo();
                            } else if (line.contains(BARCODE_START)) { //Barcode found. Print barcode.

                                try {
                                    // Log.d(null, "msg: " + message);
                                    String barcodeData = extractData(message, BARCODE_START, BARCODE_END);
                                    String barcodeType = extractData(barcodeData, TYPE_START, TYPE_END);
                                    String barcodePayload = extractData(barcodeData, PAYLOAD_START, PAYLOAD_END);
                                    int barcodeTypeInt = (Integer) barcodeTypeMap().get(barcodeType);
                                    // Log.d(null, "barcode: " + barcodeType + barcodePayload);
                                    printBarcode(barcodeTypeInt, barcodePayload);
                                    xy = bloc; //Stop trying to print the barcode if it's more than 32 characters.
                                } catch (Exception e) {
                                    Log.d(null, "com.flash.print.FlashWintecPrinter died while printing barcode.");
                                    context.error("Barcode print failed.");
                                }
                            } else { //This block is if we're just printing text.

                                printer.PRN_Alignment(BITMAP_ALIGN_LEFT); //Print from the left.
                                { //Printing actually happens here.
                                    if (line.contains(BOLD_TAG) || this.fontSize.equals("1")) {
                                        printer.PRN_EnableBoldFont(1);
                                        printLine(foo);
                                        printer.PRN_EnableBoldFont(0);
                                    } else if (line.contains(HEADER1_TAG) || this.fontSize.equals("3")) {
                                        printer.PRN_EnableDoubleWidthandHeight();
                                        printLine(foo);
                                        printer.PRN_DisableDoubleWidthandHeight();
                                    } else if (line.contains(HEADER2_TAG) || this.fontSize.equals("2")) {
                                        printer.PRN_EnableFontDoubleHeight();
                                        printLine(foo);
                                        printer.PRN_DisableFontDoubleHeight();
                                    } else {
                                        printLine(foo);
                                    }
                                }
                                // printer.PRN_Print(foo, PrinterHelper.CharacterSet); //Print now.

                            }

                            try {
                                Log.d(null, "sup");
                                Thread.sleep(250); //Wait.
                                // Log.d(null, "Waiting");
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                                Log.d(null, e.toString());
                            }
                        }
                        indices++;
                    }

                    printer.PRN_PrintAndFeedLine(BOTTOM_LINES); //Bottom lines on every receipt.
                    printer.PRN_HalfCutPaper();
                } else { //Printer has no paper else
                    callbackContext.error("Printer is out of paper!");
                }

            } else { //Incorrect arguments else
                callbackContext.error("Expected one non-empty string argument.");
            }

        } //Was printer initialised
    }

    private void printLine(String line) {
        // Log.d(null, "foo: " + line);
        printer.PRN_Alignment(BITMAP_ALIGN_LEFT);
        printer.PRN_Print(line, CHARSET);
    }

    private void printLogo() {
        try {
            printer.PRN_HalfCutPaper();
            printer.PRN_PrintDotBitmap1(LOGO_PATH, BITMAP_ALIGN_CENTER, BITMAP_SIZE);
            printer.PRN_PrintAndFeedLine(HEADER_GAP);
        } catch (Exception e) {
            String stackTrace = Log.getStackTraceString(e);
            context.error(stackTrace);
        }
    }

    private void printBarcode(int barcodeType, String payload) {
        if (this.printBarcode) {
            int printsize = BARCODE_WIDTH;
            if (barcodeType == ((Integer) barcodeTypeMap().get("ITF"))) {
                printsize /= 2;
            }

            if (deviceType.equals("TouchPro")) {
                printer.PRN_SetBarCodeWidth(printsize * 2);
                printer.PRN_SetBarCodeHeight(BARCODE_HEIGHT * 2);
            } else {
                printer.PRN_SetBarCodeAera(BARCODE_AREA_LENGTH, BARCORE_AREA_WIDTH);
                printer.PRN_SetBarCodeHeight(BARCODE_HEIGHT);

                printer.PRN_SetBarCodeWidth(printsize);
            }
            printer.PRN_SetBarCodeHRI(HRI_BOTTOM);

            printer.PRN_PrintBarCode(barcodeType, payload);
        }
        repeating = true;
        if (repeating) {
            printer.PRN_PrintAndFeedLine(LINES_AFTER_BARCODE_REPEAT_PRINT);
        }

    }

    private Map barcodeTypeMap() { //Pretty self-explanatory
        Map typeMap = new HashMap();
        typeMap.put("UPC_A", 0);
        typeMap.put("UPC_E", 1);
        typeMap.put("JAN13", 2);
        typeMap.put("JAN8", 3);
        typeMap.put("CODE39", 4);
        typeMap.put("ITF", 5);
        typeMap.put("CODABAR", 6);
        return typeMap;
    }

    //Just tells us what's between tagStart and tagEnd in msg
    private String extractData(String msg, String tagStart, String tagEnd) throws Exception {
        int x = msg.indexOf(tagStart);
        int y = msg.indexOf(tagEnd);
        if (x == -1) {
            throw new Exception("Could not find " + tagStart + "in argument. Src: FlashWintecPrinter");
        }
        if (y == -1) {
            throw new Exception("Could not find " + tagEnd + "in argument. Src: FlashWintecPrinter");
        }

        return msg.substring(x + tagStart.length(), y);
    }

}

//     printer.PRN_Alignment(0);
//     printer.PRN_Print("LINE ONE" + message, CHARSET);
//     printer.PRN_PrintAndFeedLine(3);
//     printer.PRN_Print("Line Two", CHARSET);
//     printer.PRN_PrintAndFeedLine(6);
//     printer.PRN_SetLineSpace(6);
//     printer.PRN_Print("LINE 3", CHARSET);
//     printer.PRN_SetLineSpace(1);
//     printer.PRN_Print("LINE foror", CHARSET);
//     printer.PRN_EnableBoldFont(1);
//     printer.PRN_Print("bold?", CHARSET);
//     printer.PRN_EnableBoldFont(0);
//     printer.PRN_Print("normal", CHARSET);
//     printer.PRN_EnableFontDoubleHeight();
//     printer.PRN_Print("double height", CHARSET);
//     printer.PRN_EnableFontDoubleWidth();
//     printer.PRN_Print("double height and width", CHARSET);
//     printer.PRN_DisableFontDoubleHeight();
//     printer.PRN_DisableFontDoubleWidth();

//     printer.PRN_EnableFontDoubleWidth();
//     printer.PRN_Print("just double width", CHARSET);
//     printer.PRN_DisableFontDoubleWidth();

//     printer.PRN_EnableDoubleWidthandHeight();
//     printer.PRN_Print("double w annd h again", CHARSET);
//     printer.PRN_DisableDoubleWidthandHeight();
//     printer.PRN_EnableFontUnderline();
//     printer.PRN_Print("underline?", CHARSET);
//     printer.PRN_DisableFontUnderline();

//     if (printer.PRN_GetPaperStatus())
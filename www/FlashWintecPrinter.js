var exec = require('cordova/exec');


/** \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
 *   arg0: Accepts a message, containing the string to print. We use ; as the CRLF character.
 *   <logo/> tells us to print the FLASH cow logo.
 *   <barcode> and </barcode> around a valid barcode to print it.
 *   Calls callbackContext.error() when fails and callbackContext.success() when succeeds.
 **/

// Map typeMap = new HashMap();
// typeMap.put("UPC_A", 0);
// typeMap.put("UPC_E", 1);
// typeMap.put("JAN13",2);
// typeMap.put("JAN8", 3);
// typeMap.put("CODE39", 4);
// typeMap.put("ITF", 5);
// typeMap.put("CODABAR", 6);
// return typeMap;
//<barcode><type>6</type><payload>12334!not a valid CODABAR  barcode im lazy!5345345</payload></barcode>

exports.print = function(arg0, success, error) {
    exec(success, error, "FlashWintecPrinter", "print", [arg0]);
};

/* \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
 *  @logoPath: A path (excluding /mnt/ part) to the logo that we'll be using
 *  @devicePath: The internal path to the printer (dev/ttySAC1 for instance)
 *  @printSize: An integer 0-3 representing size we want to print at
 *  @barcodeEnabled: true or false: true = print barcodes, false = disabled.
 *   returns: nothing.
 */
exports.initPrinter = function(arg0, arg1, arg2, arg3, success, error) {
    exec(success, error, "FlashWintecPrinter", "initPrinter", [arg0, arg1, arg2, arg3]);
};